var $ = Dom7;


var app = new Framework7({
  name: 'school_app', // App name
  theme: 'md', // Automatic theme detection
  el: '#app', // App root element
  view: {
		pushState: true,
	},
  id: 'com.myapp.test',
  // App store
  store: store,
  // App routes
  routes: routes,
});
var mainView = app.views.create('.view-main', {
	url: './index.html'
});
