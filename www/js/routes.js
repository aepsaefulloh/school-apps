var routes = [{
    path: '/',
    url: './index.html',
  },
  {
    name: 'absen',
    path: '/absen/',
    componentUrl: './pages/absen.html',
  },
  {
    path: '/pr/',
    url: './pages/pr.html',
  },
  {
    path: '/jadwal-pelajaran/',
    url: './pages/jadwal-pelajaran.html',
  },
  {
    path: '/nilai/',
    url: './pages/nilai.html',
  },
  {
    path: '/jadwal-ujian/',
    url: './pages/jadwal-ujian.html',
  },
  {
    path: '/bank-soal/',
    url: './pages/bank-soal.html',
  },
  {
    path: '/profile/',
    url: './pages/profile.html',
  }
];