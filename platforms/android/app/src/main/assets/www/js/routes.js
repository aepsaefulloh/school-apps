
var routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/absen/',
    url: './pages/absen.html',
  },
  {
    path: '/pr/',
    url: './pages/pr.html',
  },
  {
    path: '/jadwal-pelajaran/',
    url: './pages/jadwal-pelajaran.html',
  },
  {
    path: '/nilai/',
    url: './pages/nilai.html',
  },
  {
    path: '/jadwal-ujian/',
    url: './pages/jadwal-ujian.html',
  },
  {
    path: '/bank-soal/',
    url: './pages/bank-soal.html',
  }
];
